package com.example.newsapp.domain.usecase.news

import com.example.newsapp.data.model.news.News
import com.example.newsapp.domain.repository.news.NewsRepository

class UpdateNewsUseCase(private val newsRepository: NewsRepository) {
    suspend fun execute(): List<News> = newsRepository.updateNews()
}