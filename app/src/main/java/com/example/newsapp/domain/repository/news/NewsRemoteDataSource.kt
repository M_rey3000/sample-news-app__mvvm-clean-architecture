package com.example.newsapp.domain.repository.news

import com.example.newsapp.data.model.news.NewsList
import retrofit2.Response

interface NewsRemoteDataSource {
    suspend fun getNews(): Response<NewsList>
}