package com.example.newsapp.domain.repository.news

import com.example.newsapp.data.model.news.News
import com.example.newsapp.util.Resource

interface NewsRepository {
    suspend fun getNews(): Resource<List<News>>
    suspend fun updateNews(): List<News>
}