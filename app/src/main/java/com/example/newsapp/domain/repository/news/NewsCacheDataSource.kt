package com.example.newsapp.domain.repository.news

import com.example.newsapp.data.model.news.News

interface NewsCacheDataSource {
    suspend fun getNewsFromCache(): List<News>
    suspend fun saveNewsToCache(newsLists: List<News>)
}