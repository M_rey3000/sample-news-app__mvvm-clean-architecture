package com.example.newsapp.domain.usecase.news

import com.example.newsapp.data.model.news.News
import com.example.newsapp.domain.repository.news.NewsRepository
import com.example.newsapp.util.Resource

class GetNewsUseCase(private val newsRepository: NewsRepository) {
    suspend fun execute(): Resource<List<News>> = newsRepository.getNews()
}