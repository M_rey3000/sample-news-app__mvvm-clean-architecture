package com.example.newsapp.domain.repository.news

import com.example.newsapp.data.model.news.News

interface NewsLocalDataSource {
    suspend fun getNewsFromDB(): List<News>
    suspend fun saveNewsToDB(news: List<News>)
    suspend fun clearAll()
}