package com.example.newsapp.data.repository.news

import android.util.Log
import com.example.newsapp.data.model.news.News
import com.example.newsapp.domain.repository.news.NewsCacheDataSource
import com.example.newsapp.domain.repository.news.NewsLocalDataSource
import com.example.newsapp.domain.repository.news.NewsRemoteDataSource
import com.example.newsapp.domain.repository.news.NewsRepository
import com.example.newsapp.util.Resource
import retrofit2.HttpException

class NewsRepositoryImpl(
    private val newsRemoteDataSource: NewsRemoteDataSource,
    private val newsLocalDataSource: NewsLocalDataSource,
    private val newsCacheDataSource: NewsCacheDataSource
) : NewsRepository {

    override suspend fun getNews(): Resource<List<News>> = getNewsFromCache()

    override suspend fun updateNews(): List<News> {
        val newsFromApi = getNewsFromAPI().data!!
        newsLocalDataSource.clearAll()
        newsLocalDataSource.saveNewsToDB(newsFromApi)
        newsCacheDataSource.saveNewsToCache(newsFromApi)
        return newsFromApi
    }

    private suspend fun getNewsFromAPI(): Resource<List<News>> {
        var newsList: Resource<List<News>> = Resource.error(null)
        try {
            val response = newsRemoteDataSource.getNews()
            val body = response.body()
            Log.d("RESPONSE", response.body().toString())
            if (body != null && response.isSuccessful) {
                newsList = Resource.success(body.articles!!)
            }

        } catch (e: HttpException) {
            e.printStackTrace()
            newsList = Resource.httpError(e)
        } catch (e: Exception) {
            e.printStackTrace()
            newsList = Resource.error(e)
        }

        return newsList
    }

    private suspend fun getNewsFromDB(): Resource<List<News>> {
        var newsList: Resource<List<News>> = Resource.error(null)
        newsList = try {
            Resource.success(newsLocalDataSource.getNewsFromDB())

        } catch (e: Exception) {
            e.printStackTrace()
            Resource.error(e)
        }

        if (newsList.data != null) {
            if (newsList.data!!.isNotEmpty())
                return newsList
            else {
                newsList = getNewsFromAPI()
                newsLocalDataSource.saveNewsToDB(newsList.data!!)
            }
        }

        return newsList
    }

    private suspend fun getNewsFromCache(): Resource<List<News>> {
        var newsList: Resource<List<News>> = Resource.error(null)
        newsList = try {
            Resource.success(newsCacheDataSource.getNewsFromCache())
        } catch (e: Exception) {
            e.printStackTrace()
            Resource.error(e)
        }

        if (newsList.data != null) {
            if (newsList.data!!.isNotEmpty())
                return newsList
            else {
                newsList = getNewsFromDB()
                newsCacheDataSource.saveNewsToCache(newsList.data!!)
            }
        }
        return newsList
    }
}