package com.example.newsapp.data.repository.news

import com.example.newsapp.data.api.NewsService
import com.example.newsapp.data.model.news.NewsList
import com.example.newsapp.domain.repository.news.NewsRemoteDataSource
import retrofit2.Response

class NewsRemoteDataSourceImpl(private val service: NewsService) : NewsRemoteDataSource {
    override suspend fun getNews(): Response<NewsList> = service.getNews()
}