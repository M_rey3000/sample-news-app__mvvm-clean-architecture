package com.example.newsapp.data.repository.news

import com.example.newsapp.data.db.news.NewsDAO
import com.example.newsapp.data.model.news.News
import com.example.newsapp.domain.repository.news.NewsLocalDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewsLocalDataSourceImpl(private val newsDAO: NewsDAO) : NewsLocalDataSource {

    // Room will use background thread for fetching data automatically
    override suspend fun getNewsFromDB(): List<News> = newsDAO.getAllNews()


    override suspend fun saveNewsToDB(news: List<News>) {
        CoroutineScope(Dispatchers.IO).launch {
            newsDAO.saveAllNews(news)
        }
    }

    override suspend fun clearAll() {
        CoroutineScope(Dispatchers.IO).launch {
            newsDAO.deleteAllNews()
        }
    }
}