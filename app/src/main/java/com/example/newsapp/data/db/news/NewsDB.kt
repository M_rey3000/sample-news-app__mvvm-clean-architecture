package com.example.newsapp.data.db.news

import androidx.room.*
import com.example.newsapp.data.model.news.News
import com.example.newsapp.data.model.news.Source

@Database(entities = [News::class], version = 1)
@TypeConverters(Converters::class)
abstract class NewsDB : RoomDatabase() {
    abstract val newsDAO: NewsDAO
}

class Converters {
    @TypeConverter
    fun fromSource(source: Source?): String? {
        return source?.let { source.name }
    }
}
