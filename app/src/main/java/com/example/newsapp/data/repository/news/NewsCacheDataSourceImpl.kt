package com.example.newsapp.data.repository.news

import com.example.newsapp.data.model.news.News
import com.example.newsapp.domain.repository.news.NewsCacheDataSource

class NewsCacheDataSourceImpl : NewsCacheDataSource {

    private var newsList = ArrayList<News>()
    override suspend fun getNewsFromCache(): List<News> = newsList

    override suspend fun saveNewsToCache(news: List<News>) {
        newsList.clear()
        newsList = ArrayList(news)
    }
}