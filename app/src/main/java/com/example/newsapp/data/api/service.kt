package com.example.newsapp.data.api

import com.example.newsapp.BuildConfig
import com.example.newsapp.data.model.news.NewsList
import com.google.gson.GsonBuilder
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query


interface NewsService {
    @GET("/v2/everything")
    suspend fun getNews(
        @Header("Authorization") api: String = BuildConfig.API_KEY,
//        @Query("pageSize") pageSize: Int = NewsRecycleViewAdapter.pageSize,
//        @Query("page") page: Int = NewsRecycleViewAdapter.pageToLoad,
        @Query("q") query: String = "keyword"
    ): Response<NewsList>
}

