package com.example.newsapp.util

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import retrofit2.HttpException

sealed class ResourceHandler<T>(val data: T? = null, val message: String? = null) {
    class Success<T>(data: T) : ResourceHandler<T>(data)
    class Error<T>(message: String, data: T? = null) : ResourceHandler<T>(data, message)
    class Loading<T>(data: T? = null) : ResourceHandler<T>(data)
}


/**
 * Use this class to post deferred values and errors to LiveData<WebResource<Deferred>>
 * And observe in our models
 */
class Resource<T> private constructor(
    val status: Status,
    val data: T?,
    val exception: Exception?,
    val httpException: HttpException?
) {
    enum class Status {
        SUCCESS, ERROR, HTTP_ERROR, LOADING
    }

    companion object {
        fun <T> success(@NonNull data: T): Resource<T> {
            return Resource(Status.SUCCESS, data, null, null)
        }

        fun <T> httpError(httpException: HttpException): Resource<T> {
            return Resource(Status.HTTP_ERROR, null, null, httpException)
        }

        fun <T> error(ex: Exception?): Resource<T> {
            return Resource(Status.ERROR, null, ex, null)
        }

        fun <T> loading(@Nullable data: T): Resource<T> {
            return Resource(Status.LOADING, data, null, null)
        }
    }
}
