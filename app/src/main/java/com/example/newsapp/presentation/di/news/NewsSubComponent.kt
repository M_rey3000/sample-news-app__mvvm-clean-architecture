package com.example.newsapp.presentation.di.news

import com.example.newsapp.presentation.view.MainActivity
import com.example.newsapp.presentation.view.fragments.NewsFragment
import dagger.Subcomponent

@NewsScope
@Subcomponent(modules = [NewsModule::class])
interface NewsSubComponent {
    fun inject(mainActivity: MainActivity)

    fun inject(newsFragment: NewsFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(): NewsSubComponent
    }
}