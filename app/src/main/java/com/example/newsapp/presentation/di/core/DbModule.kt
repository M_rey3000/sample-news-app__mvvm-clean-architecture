package com.example.newsapp.presentation.di.core

import android.content.Context
import androidx.room.Room
import com.example.newsapp.data.db.news.NewsDAO
import com.example.newsapp.data.db.news.NewsDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Singleton
    @Provides
    fun provideNewsDb(context: Context): NewsDB =
        Room.databaseBuilder(
            context.applicationContext,
            NewsDB::class.java, "news_db"
        ).build()

    @Singleton
    @Provides
    fun provideNewsDao(newsDB: NewsDB): NewsDAO = newsDB.newsDAO

}