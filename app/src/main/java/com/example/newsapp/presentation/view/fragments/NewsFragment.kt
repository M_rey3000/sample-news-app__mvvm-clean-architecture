package com.example.newsapp.presentation.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.newsapp.databinding.FragmentNewsBinding
import com.example.newsapp.presentation.di.Injector
import com.example.newsapp.presentation.view.adapter.NewsRecycleViewAdapter
import com.example.newsapp.presentation.viewmodel.NewsViewModel
import com.example.newsapp.presentation.viewmodel.NewsViewModelFactory
import com.example.newsapp.util.Resource
import javax.inject.Inject


class NewsFragment : Fragment() {

    @Inject
    lateinit var factory: NewsViewModelFactory
    private lateinit var newsViewModel: NewsViewModel

    private lateinit var binding: FragmentNewsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (requireActivity().application as Injector).createNewsSubComponent()
            .inject(this)

        newsViewModel = ViewModelProvider(this, factory)[NewsViewModel::class.java]

        binding = FragmentNewsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycleView()
    }


    private fun initRecycleView() {
        val newsRecycleViewAdapter = NewsRecycleViewAdapter()
        binding.newsList.adapter = newsRecycleViewAdapter


        val news = newsViewModel.getNews()

        news.observe(viewLifecycleOwner, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    it.data?.let { it1 -> newsRecycleViewAdapter.setList(it1) }
                }
                Resource.Status.ERROR, Resource.Status.HTTP_ERROR -> {
                    Toast.makeText(context, "No connection!", Toast.LENGTH_SHORT).show()
                }
                else -> Toast.makeText(context, "No connection!", Toast.LENGTH_SHORT).show()

            }

        })

        newsRecycleViewAdapter.onItemClick = {
            Navigation.findNavController(binding.root)
                .navigate(NewsFragmentDirections.actionNewsFragmentToNewsDetailFragment(it))
        }

    }

}