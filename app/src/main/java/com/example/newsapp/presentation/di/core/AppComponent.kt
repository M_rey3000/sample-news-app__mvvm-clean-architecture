package com.example.newsapp.presentation.di.core

import com.example.newsapp.presentation.di.news.NewsSubComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        RemoteDataSourceModule::class,
        LocalDataSourceModule::class,
        CacheDataSourceModule::class,
        DbModule::class,
        NetModule::class,
        RepositoryModule::class,
        UseCaseModule::class
    ]
)
interface AppComponent {
    fun newsSubComponent(): NewsSubComponent.Factory
}