package com.example.newsapp.presentation.di.core

import android.content.Context
import com.example.newsapp.presentation.di.news.NewsSubComponent
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(subcomponents = [NewsSubComponent::class])
class AppModule(private val context: Context) {

    @Singleton
    @Provides
    fun provideAppContext(): Context = context.applicationContext
}