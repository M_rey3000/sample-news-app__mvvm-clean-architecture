package com.example.newsapp.presentation.di.core

import com.example.newsapp.data.api.NewsService
import com.example.newsapp.data.db.news.NewsDAO
import com.example.newsapp.data.repository.news.NewsCacheDataSourceImpl
import com.example.newsapp.data.repository.news.NewsLocalDataSourceImpl
import com.example.newsapp.data.repository.news.NewsRemoteDataSourceImpl
import com.example.newsapp.domain.repository.news.NewsCacheDataSource
import com.example.newsapp.domain.repository.news.NewsLocalDataSource
import com.example.newsapp.domain.repository.news.NewsRemoteDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteDataSourceModule {
    @Singleton
    @Provides
    fun provideNewsRemoteDataSource(newsService: NewsService): NewsRemoteDataSource =
        NewsRemoteDataSourceImpl(newsService)
}

@Module
class LocalDataSourceModule {
    @Singleton
    @Provides
    fun provideNewsLocalDataSource(newsDAO: NewsDAO): NewsLocalDataSource =
        NewsLocalDataSourceImpl(newsDAO)

}

@Module
class CacheDataSourceModule {
    @Singleton
    @Provides
    fun provideNewsLocalDataSource(): NewsCacheDataSource =
        NewsCacheDataSourceImpl()

}