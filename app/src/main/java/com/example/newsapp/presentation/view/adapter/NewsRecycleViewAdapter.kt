package com.example.newsapp.presentation.view.adapter


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsapp.R
import com.example.newsapp.data.model.news.News
import com.example.newsapp.databinding.NewsItemBinding

class NewsRecycleViewAdapter : RecyclerView.Adapter<NewsRecycleViewAdapter.ViewHolder>() {
    private val newsList = ArrayList<News>()

    var onItemClick: ((News) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: NewsItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.news_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(newsList[position])
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setList(news: List<News>) {
        //newsList.clear()
        newsList.addAll(news)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: NewsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.cvNews.setOnClickListener {
                onItemClick?.invoke(newsList[adapterPosition])
            }
        }

        fun bind(news: News) {
            binding.news = news
            binding.executePendingBindings()

        }

    }

    companion object {
        @JvmStatic
        @BindingAdapter("bind:loadNewsIcon")
        fun ImageView.loadNewsIcon(newsTemplate: String) {
            Glide.with(context).load(newsTemplate)
                .placeholder(R.drawable.ic_launcher_background)
                .into(this)
        }
    }
}
