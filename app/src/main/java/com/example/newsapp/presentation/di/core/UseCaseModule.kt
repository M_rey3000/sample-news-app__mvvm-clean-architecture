package com.example.newsapp.presentation.di.core

import com.example.newsapp.domain.repository.news.NewsRepository
import com.example.newsapp.domain.usecase.news.GetNewsUseCase
import com.example.newsapp.domain.usecase.news.UpdateNewsUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {
    @Singleton
    @Provides
    fun provideGetNewsUseCaseModule(newsRepository: NewsRepository): GetNewsUseCase =
        GetNewsUseCase(newsRepository)

    @Singleton
    @Provides
    fun provideUpdateNewsUseCaseModule(newsRepository: NewsRepository): UpdateNewsUseCase =
        UpdateNewsUseCase(newsRepository)
}