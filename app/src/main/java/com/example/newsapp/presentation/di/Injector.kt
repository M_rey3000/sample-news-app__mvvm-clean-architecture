package com.example.newsapp.presentation.di

import com.example.newsapp.presentation.di.news.NewsSubComponent

interface Injector {
    fun createNewsSubComponent(): NewsSubComponent
}