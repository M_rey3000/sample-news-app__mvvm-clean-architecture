package com.example.newsapp.presentation.di.core

import com.example.newsapp.data.repository.news.NewsRepositoryImpl
import com.example.newsapp.domain.repository.news.NewsCacheDataSource
import com.example.newsapp.domain.repository.news.NewsLocalDataSource
import com.example.newsapp.domain.repository.news.NewsRemoteDataSource
import com.example.newsapp.domain.repository.news.NewsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideNewsRepositoryModule(
        newsRemoteDataSource: NewsRemoteDataSource,
        newsLocalDataSource: NewsLocalDataSource,
        newsCacheDataSource: NewsCacheDataSource
    ): NewsRepository =
        NewsRepositoryImpl(newsRemoteDataSource, newsLocalDataSource, newsCacheDataSource)
}